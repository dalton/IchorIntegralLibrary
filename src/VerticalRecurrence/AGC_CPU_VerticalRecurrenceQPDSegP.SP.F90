module SPAGC_CPU_OBS_VERTICALRECURRENCEMODDSegP
 use IchorPrecisionMod
  
 CONTAINS


subroutine SPVerticalRecurrenceCPUSegP1D(nPassP,nPrimP,nPrimQ,&
         & reducedExponents,TABFJW,Qexp,Dcenter,Pcent,Qcent,integralPrefactor,&
         & IatomApass,IatomBpass,MaxPasses,nAtomsA,nAtomsB,&
         & PpreExpFac,QpreExpFac,AUXarray)
  implicit none
  integer,intent(in) :: nPassP,nPrimP,nPrimQ
  integer,intent(in) :: MaxPasses,nAtomsA,nAtomsB
  integer,intent(in) :: IatomApass(MaxPasses),IatomBpass(MaxPasses)
  real(reals),intent(in) :: TABFJW(0:4,0:1200)
  real(reals),intent(in) :: reducedExponents(nPrimQ,nPrimP),Qexp(nPrimQ)
  real(reals),intent(in) :: Pcent(3,nPrimP,nAtomsA,nAtomsB),Qcent(3,nPrimQ)
  real(reals),intent(in) :: integralPrefactor(nprimQ,nPrimP),QpreExpFac(nPrimQ),PpreExpFac(nPrimP,nAtomsA,nAtomsB)
  real(reals),intent(in) :: Dcenter(3)
  real(reals),intent(inout) :: AUXarray(4,nPrimQ*nPassP)
  !local variables
  Integer :: iP,iPrimQ,iPrimP,iPassP,ipnt,iAtomA,iAtomB
  real(reals) :: Xqd,Yqd,Zqd
  real(reals) :: mPX,mPY,mPZ,invexpQ,alphaQ,RJ000(0:1)
  real(reals) :: PREF,TMP1,TMP2,Xpq,Ypq,Zpq,alphaXpq,alphaYpq,alphaZpq
  real(reals) :: squaredDistance,WVAL,WDIFF,W2,W3,REXPW,RWVAL,GVAL
  real(reals),PARAMETER :: TENTH = 0.01E0_reals,D05 =0.5E0_reals
  real(reals),parameter :: D2=2.0E0_reals
  real(reals),PARAMETER :: D2JP36=  3.8000000000000000E+01_reals
  real(reals),parameter :: D1=1.0E0_reals,D03333=1.0E0_reals/3.0E0_reals
  real(reals),PARAMETER :: D4 = 4E0_reals, D100=100E0_reals
  real(reals),PARAMETER :: COEF3 = - D1/6E0_reals, COEF4 = D1/24E0_reals
  real(reals),PARAMETER :: SMALL = 1E-15_reals,D12 = 12.0E0_reals
  real(reals), PARAMETER :: GFAC0 =  D2*0.4999489092E0_reals
  real(reals), PARAMETER :: GFAC1 = -D2*0.2473631686E0_reals
  real(reals), PARAMETER :: GFAC2 =  D2*0.321180909E0_reals
  real(reals), PARAMETER :: GFAC3 = -D2*0.3811559346E0_reals
  real(reals), parameter :: PI=3.14159265358979323846E0_reals
  real(reals), PARAMETER :: SQRTPI = 1.77245385090551602730E00_reals
  real(reals), PARAMETER :: SQRPIH = SQRTPI/D2
  real(reals), PARAMETER :: PID4 = PI/D4, PID4I = D4/PI
  !ThetaAux(n,1,0,0) = Xqd*ThetaAux(n,0,0,0) + (-alpha/q*Xpq)*ThetaAux(n+1,0,0,0)
  !i = 0 last 2 term vanish
  !We include scaling of RJ000 
!$OMP DO PRIVATE(iP)
  DO iP = 1,nPrimQ*nPassP
    AUXarray(1,iP)=0.0E0_reals
    AUXarray(2,iP)=0.0E0_reals
    AUXarray(3,iP)=0.0E0_reals
    AUXarray(4,iP)=0.0E0_reals
  ENDDO
!$OMP END DO
!$OMP DO &
!$OMP PRIVATE(iAtomA,iAtomB,Xpq,Ypq,Zpq,&
!$OMP         squaredDistance,WVAL,IPNT,WDIFF,W2,W3,RJ000,REXPW,&
!$OMP         RWVAL,GVAL,&
!$OMP         mPx,mPy,mPz,&
!$OMP         Xqd,Yqd,Zqd,alphaQ,&
!$OMP         alphaXpq,alphaYpq,alphaZpq,&
!$OMP         invexpQ,&
!$OMP         PREF,&
!$OMP         TMP1,TMP2,&
!$OMP         iP,iPrimQ,iPrimP,iPassP)
  DO iP = 1,nPrimQ*nPassP
   DO iPrimP=1, nPrimP
    iPrimQ = iP - ((iP-1)/nPrimQ)*nPrimQ
    iPassP = (iP-1)/nPrimQ + 1
   iAtomA = iAtomApass(iPassP)
   iAtomB = iAtomBpass(iPassP)
    mPX = -Pcent(1,iPrimP,iAtomA,iAtomB)
    mPY = -Pcent(2,iPrimP,iAtomA,iAtomB)
    mPZ = -Pcent(3,iPrimP,iAtomA,iAtomB)
    invexpQ = D1/Qexp(iPrimQ)
   Xpq = mPX + Qcent(1,iPrimQ)
   Ypq = mPY + Qcent(2,iPrimQ)
   Zpq = mPZ + Qcent(3,iPrimQ)
     Xqd = Qcent(1,iPrimQ) - Dcenter(1)
     Yqd = Qcent(2,iPrimQ) - Dcenter(2)
     Zqd = Qcent(3,iPrimQ) - Dcenter(3)
     alphaQ = -reducedExponents(iPrimQ,iPrimP)*invexpQ
     alphaXpq = alphaQ*Xpq
     alphaYpq = alphaQ*Ypq
     alphaZpq = alphaQ*Zpq
     squaredDistance = Xpq*Xpq+Ypq*Ypq+Zpq*Zpq
     WVAL = reducedExponents(iPrimQ,iPrimP)*squaredDistance
     IF (WVAL .LT. D12) THEN
      IPNT = NINT(D100*WVAL)
      WDIFF = WVAL - TENTH*IPNT
      W2    = WDIFF*WDIFF
      W3    = W2*WDIFF
      W2    = W2*D05
      W3    = W3*COEF3
      RJ000(0)=TABFJW(0,IPNT)-TABFJW(1,IPNT)*WDIFF+TABFJW(2,IPNT)*W2+TABFJW(3,IPNT)*W3
      RJ000(1)=TABFJW(1,IPNT)-TABFJW(2,IPNT)*WDIFF+TABFJW(3,IPNT)*W2+TABFJW(4,IPNT)*W3
     !  12 < WVAL <= (2J+36) 
     ELSE IF (WVAL.LE.D2JP36) THEN
      REXPW = D05*EXP(-WVAL)
      RWVAL = D1/WVAL
      GVAL  = GFAC0 + RWVAL*(GFAC1 + RWVAL*(GFAC2 + RWVAL*GFAC3))
      RJ000(0) = SQRPIH*SQRT(RWVAL) - REXPW*GVAL*RWVAL
      RJ000(1) = RWVAL*(D05*RJ000(0)-REXPW)
     !  (2J+36) < WVAL 
     ELSE
      RWVAL = PID4/WVAL
      RJ000(0) = SQRT(RWVAL)
      RJ000(1) = RWVAL*PID4I*D05*RJ000(0)
     ENDIF
     PREF = integralPrefactor(iPrimQ,iPrimP)*QpreExpFac(iPrimQ)*PpreExpFac(iPrimP,iAtomA,iAtomB)
     TMP1 = PREF*RJ000(0)
     TMP2 = PREF*RJ000(1)
     AUXarray(1,iP) = AUXarray(1,iP) + TMP1
     AUXarray(2,iP) = AUXarray(2,iP) + Xqd*TMP1 + alphaXpq*TMP2
     AUXarray(3,iP) = AUXarray(3,iP) + Yqd*TMP1 + alphaYpq*TMP2
     AUXarray(4,iP) = AUXarray(4,iP) + Zqd*TMP1 + alphaZpq*TMP2
   ENDDO !iPrimQ=1, nPrimQ
  ENDDO !iP = 1,nPrimP*nPassP
!$OMP END DO
end subroutine SPVerticalRecurrenceCPUSegP1D

subroutine SPVerticalRecurrenceCPUSegP2D(nPassP,nPrimP,nPrimQ,&
         & reducedExponents,RJ000Array,Qexp,Dcenter,Pcent,Qcent,integralPrefactor,&
         & IatomApass,IatomBpass,MaxPasses,nAtomsA,nAtomsB,PpreExpFac,QpreExpFac,AUXarray)
  implicit none
  integer,intent(in) :: nPassP,nPrimP,nPrimQ
  integer,intent(in) :: MaxPasses,nAtomsA,nAtomsB
  integer,intent(in) :: IatomApass(MaxPasses),IatomBpass(MaxPasses)
  real(reals),intent(in) :: RJ000Array(0: 2,nPrimQ,nPrimP,nPassP)
  real(reals),intent(in) :: reducedExponents(nPrimQ,nPrimP),Qexp(nPrimQ)
  real(reals),intent(in) :: Pcent(3,nPrimP,nAtomsA,nAtomsB),Qcent(3,nPrimQ)
  real(reals),intent(in) :: integralPrefactor(nprimQ,nPrimP),QpreExpFac(nPrimQ),PpreExpFac(nPrimP,nAtomsA,nAtomsB)
  real(reals),intent(in) :: Dcenter(3)
  real(reals),intent(inout) :: AUXarray(   10,nPrimQ*nPassP)
  !local variables
  integer :: iPassP,iPrimP,iPrimQ,ipnt,IP,iTUV,iAtomA,iAtomB
  real(reals) :: TMPAUXarray(    4)
  real(reals) :: Xqd,Yqd,Zqd
  real(reals) :: mPX,mPY,mPZ,invexpQ,inv2expQ,alphaQ
  real(reals) :: TwoTerms(   1)
  real(reals) :: PREF,TMP1,TMP2,Xpq,Ypq,Zpq,alphaXpq,alphaYpq,alphaZpq
  real(reals),parameter :: D2=2.0E0_reals,D05 =0.5E0_reals
  real(reals),parameter :: D1=1.0E0_reals
  real(reals) :: TMParray1(  1:  1,2:3)
  real(reals) :: TMParray2(  2:  4,2:2)
  !TUV(T,0,0,N) = Xqd*TUV(T-1,0,0,N)-(alpha/q)*Xpq*TUV(T-1,0,0,N+1)
  !             + T/(2q)*(TUV(T-2,0,0,N)-(alpha/q)*TUV(T-2,0,0,N+1))
  !We include scaling of RJ000 
!$OMP DO PRIVATE(iP,iTUV)
  DO iP = 1,nPrimQ*nPassP
    DO iTUV=1,   10
     AUXarray(iTUV,iP)=0.0E0_reals
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO &
!$OMP PRIVATE(iAtomA,iAtomB,Xpq,Ypq,Zpq,&
!$OMP         mPx,mPy,mPz,&
!$OMP         Xqd,Yqd,Zqd,alphaQ,&
!$OMP         alphaXpq,alphaYpq,alphaZpq,&
!$OMP         invexpQ,inv2expQ,&
!$OMP         PREF,&
!$OMP         TMPAUXarray,&
!$OMP         TMParray1,&
!$OMP         TMParray2,&
!$OMP         TwoTerms,&
!$OMP         iP,iPrimQ,iPrimP,iPassP)
  DO iP = 1,nPrimQ*nPassP
   DO iPrimP=1, nPrimP
    iPrimQ = iP - ((iP-1)/nPrimQ)*nPrimQ
    iPassP = (iP-1)/nPrimQ + 1
     iAtomA = iAtomApass(iPassP)
     iAtomB = iAtomBpass(iPassP)
    mPX = -Pcent(1,iPrimP,iAtomA,iAtomB)
    mPY = -Pcent(2,iPrimP,iAtomA,iAtomB)
    mPZ = -Pcent(3,iPrimP,iAtomA,iAtomB)
     invexpQ = D1/Qexp(iPrimQ)
     inv2expQ = D05*invexpQ
     Xpq = mPX + Qcent(1,iPrimQ)
     Ypq = mPY + Qcent(2,iPrimQ)
     Zpq = mPZ + Qcent(3,iPrimQ)
     Xqd = Qcent(1,iPrimQ) - Dcenter(1)
     Yqd = Qcent(2,iPrimQ) - Dcenter(2)
     Zqd = Qcent(3,iPrimQ) - Dcenter(3)
     alphaQ = -reducedExponents(iPrimQ,iPrimP)*invexpQ
     alphaXpq = alphaQ*Xpq
     alphaYpq = alphaQ*Ypq
     alphaZpq = alphaQ*Zpq
     PREF = integralPrefactor(iPrimQ,iPrimP)*QpreExpFac(iPrimQ)*PpreExpFac(iPrimP,iAtomA,iAtomB)
     TMPAuxarray(1) = PREF*RJ000Array(0,iPrimQ,iPrimP,iPassP)
     TMParray1(1, 2) = PREF*RJ000Array( 1,iPrimQ,iPrimP,iPassP)
     TMParray1(1, 3) = PREF*RJ000Array( 2,iPrimQ,iPrimP,iPassP)
     TMPAuxArray(2) = Xqd*TMPAuxArray(1) + alphaXpq*TmpArray1(1,2)
     TMPAuxArray(3) = Yqd*TMPAuxArray(1) + alphaYpq*TmpArray1(1,2)
     TMPAuxArray(4) = Zqd*TMPAuxArray(1) + alphaZpq*TmpArray1(1,2)
     tmpArray2(2,2) = Xqd*tmpArray1(1,2) + alphaXpq*TmpArray1(1,3)
     tmpArray2(3,2) = Yqd*tmpArray1(1,2) + alphaYpq*TmpArray1(1,3)
     tmpArray2(4,2) = Zqd*tmpArray1(1,2) + alphaZpq*TmpArray1(1,3)
     TwoTerms(1) = inv2expQ*(TMPAuxArray(1) + alphaQ*TmpArray1(1,2))
     do iTUV = 1,    4
      AuxArray(iTUV,iP) = AuxArray(iTUV,iP) + TMPAuxarray(iTUV)
     enddo
     AuxArray(5,iP) = AuxArray(5,iP) + Xqd*TMPAuxArray(2) + alphaXpq*TmpArray2(2,2) + TwoTerms(1)
     AuxArray(6,iP) = AuxArray(6,iP) + Xqd*TMPAuxArray(3) + alphaXpq*TmpArray2(3,2)
     AuxArray(7,iP) = AuxArray(7,iP) + Xqd*TMPAuxArray(4) + alphaXpq*TmpArray2(4,2)
     AuxArray(8,iP) = AuxArray(8,iP) + Yqd*TMPAuxArray(3) + alphaYpq*TmpArray2(3,2) + TwoTerms(1)
     AuxArray(9,iP) = AuxArray(9,iP) + Yqd*TMPAuxArray(4) + alphaYpq*TmpArray2(4,2)
     AuxArray(10,iP) = AuxArray(10,iP) + Zqd*TMPAuxArray(4) + alphaZpq*TmpArray2(4,2) + TwoTerms(1)
   ENDDO !iPrimQ=1, nPrimQ
  ENDDO !iP = 1,nPrimP*nPassP
!$OMP END DO
 end subroutine

subroutine SPVerticalRecurrenceCPUSegP3D(nPassP,nPrimP,nPrimQ,&
         & reducedExponents,RJ000Array,Qexp,Dcenter,Pcent,Qcent,integralPrefactor,&
         & IatomApass,IatomBpass,MaxPasses,nAtomsA,nAtomsB,PpreExpFac,QpreExpFac,AUXarray)
  implicit none
  integer,intent(in) :: nPassP,nPrimP,nPrimQ
  integer,intent(in) :: MaxPasses,nAtomsA,nAtomsB
  integer,intent(in) :: IatomApass(MaxPasses),IatomBpass(MaxPasses)
  real(reals),intent(in) :: RJ000Array(0: 3,nPrimQ,nPrimP,nPassP)
  real(reals),intent(in) :: reducedExponents(nPrimQ,nPrimP),Qexp(nPrimQ)
  real(reals),intent(in) :: Pcent(3,nPrimP,nAtomsA,nAtomsB),Qcent(3,nPrimQ)
  real(reals),intent(in) :: integralPrefactor(nprimQ,nPrimP),QpreExpFac(nPrimQ),PpreExpFac(nPrimP,nAtomsA,nAtomsB)
  real(reals),intent(in) :: Dcenter(3)
  real(reals),intent(inout) :: AUXarray(   20,nPrimQ*nPassP)
  !local variables
  integer :: iPassP,iPrimP,iPrimQ,ipnt,IP,iTUV,iAtomA,iAtomB
  real(reals) :: TMPAUXarray(   10)
  real(reals) :: Xqd,Yqd,Zqd
  real(reals) :: mPX,mPY,mPZ,invexpQ,inv2expQ,alphaQ
  real(reals) :: TwoTerms(   3)
  real(reals) :: PREF,TMP1,TMP2,Xpq,Ypq,Zpq,alphaXpq,alphaYpq,alphaZpq
  real(reals),parameter :: D2=2.0E0_reals,D05 =0.5E0_reals
  real(reals),parameter :: D1=1.0E0_reals
  real(reals) :: TMParray1(  1:  1,2:4)
  real(reals) :: TMParray2(  2:  4,2:3)
  real(reals) :: TMParray3(  5: 10,2:2)
  !TUV(T,0,0,N) = Xqd*TUV(T-1,0,0,N)-(alpha/q)*Xpq*TUV(T-1,0,0,N+1)
  !             + T/(2q)*(TUV(T-2,0,0,N)-(alpha/q)*TUV(T-2,0,0,N+1))
  !We include scaling of RJ000 
!$OMP DO PRIVATE(iP,iTUV)
  DO iP = 1,nPrimQ*nPassP
    DO iTUV=1,   20
     AUXarray(iTUV,iP)=0.0E0_reals
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO &
!$OMP PRIVATE(iAtomA,iAtomB,Xpq,Ypq,Zpq,&
!$OMP         mPx,mPy,mPz,&
!$OMP         Xqd,Yqd,Zqd,alphaQ,&
!$OMP         alphaXpq,alphaYpq,alphaZpq,&
!$OMP         invexpQ,inv2expQ,&
!$OMP         PREF,&
!$OMP         TMPAUXarray,&
!$OMP         TMParray1,&
!$OMP         TMParray2,&
!$OMP         TMParray3,&
!$OMP         TwoTerms,&
!$OMP         iP,iPrimQ,iPrimP,iPassP)
  DO iP = 1,nPrimQ*nPassP
   DO iPrimP=1, nPrimP
    iPrimQ = iP - ((iP-1)/nPrimQ)*nPrimQ
    iPassP = (iP-1)/nPrimQ + 1
     iAtomA = iAtomApass(iPassP)
     iAtomB = iAtomBpass(iPassP)
    mPX = -Pcent(1,iPrimP,iAtomA,iAtomB)
    mPY = -Pcent(2,iPrimP,iAtomA,iAtomB)
    mPZ = -Pcent(3,iPrimP,iAtomA,iAtomB)
     invexpQ = D1/Qexp(iPrimQ)
     inv2expQ = D05*invexpQ
     Xpq = mPX + Qcent(1,iPrimQ)
     Ypq = mPY + Qcent(2,iPrimQ)
     Zpq = mPZ + Qcent(3,iPrimQ)
     Xqd = Qcent(1,iPrimQ) - Dcenter(1)
     Yqd = Qcent(2,iPrimQ) - Dcenter(2)
     Zqd = Qcent(3,iPrimQ) - Dcenter(3)
     alphaQ = -reducedExponents(iPrimQ,iPrimP)*invexpQ
     alphaXpq = alphaQ*Xpq
     alphaYpq = alphaQ*Ypq
     alphaZpq = alphaQ*Zpq
     PREF = integralPrefactor(iPrimQ,iPrimP)*QpreExpFac(iPrimQ)*PpreExpFac(iPrimP,iAtomA,iAtomB)
     TMPAuxarray(1) = PREF*RJ000Array(0,iPrimQ,iPrimP,iPassP)
     TMParray1(1, 2) = PREF*RJ000Array( 1,iPrimQ,iPrimP,iPassP)
     TMParray1(1, 3) = PREF*RJ000Array( 2,iPrimQ,iPrimP,iPassP)
     TMParray1(1, 4) = PREF*RJ000Array( 3,iPrimQ,iPrimP,iPassP)
     TMPAuxArray(2) = Xqd*TMPAuxArray(1) + alphaXpq*TmpArray1(1,2)
     TMPAuxArray(3) = Yqd*TMPAuxArray(1) + alphaYpq*TmpArray1(1,2)
     TMPAuxArray(4) = Zqd*TMPAuxArray(1) + alphaZpq*TmpArray1(1,2)
     tmpArray2(2,2) = Xqd*tmpArray1(1,2) + alphaXpq*TmpArray1(1,3)
     tmpArray2(3,2) = Yqd*tmpArray1(1,2) + alphaYpq*TmpArray1(1,3)
     tmpArray2(4,2) = Zqd*tmpArray1(1,2) + alphaZpq*TmpArray1(1,3)
     tmpArray2(2,3) = Xqd*tmpArray1(1,3) + alphaXpq*TmpArray1(1,4)
     tmpArray2(3,3) = Yqd*tmpArray1(1,3) + alphaYpq*TmpArray1(1,4)
     tmpArray2(4,3) = Zqd*tmpArray1(1,3) + alphaZpq*TmpArray1(1,4)
     TwoTerms(1) = inv2expQ*(TMPAuxArray(1) + alphaQ*TmpArray1(1,2))
     TMPAuxArray(5) = Xqd*TMPAuxArray(2) + alphaXpq*TmpArray2(2,2) + TwoTerms(1)
     TMPAuxArray(6) = Xqd*TMPAuxArray(3) + alphaXpq*TmpArray2(3,2)
     TMPAuxArray(7) = Xqd*TMPAuxArray(4) + alphaXpq*TmpArray2(4,2)
     TMPAuxArray(8) = Yqd*TMPAuxArray(3) + alphaYpq*TmpArray2(3,2) + TwoTerms(1)
     TMPAuxArray(9) = Yqd*TMPAuxArray(4) + alphaYpq*TmpArray2(4,2)
     TMPAuxArray(10) = Zqd*TMPAuxArray(4) + alphaZpq*TmpArray2(4,2) + TwoTerms(1)
     TwoTerms(1) = inv2expQ*(TmpArray1(1,2) + alphaQ*TmpArray1(1,3))
     tmpArray3(5,2) = Xqd*tmpArray2(2,2) + alphaXpq*TmpArray2(2,3) + TwoTerms(1)
     tmpArray3(6,2) = Xqd*tmpArray2(3,2) + alphaXpq*TmpArray2(3,3)
     tmpArray3(7,2) = Xqd*tmpArray2(4,2) + alphaXpq*TmpArray2(4,3)
     tmpArray3(8,2) = Yqd*tmpArray2(3,2) + alphaYpq*TmpArray2(3,3) + TwoTerms(1)
     tmpArray3(9,2) = Yqd*tmpArray2(4,2) + alphaYpq*TmpArray2(4,3)
     tmpArray3(10,2) = Zqd*tmpArray2(4,2) + alphaZpq*TmpArray2(4,3) + TwoTerms(1)
     TwoTerms(1) = inv2expQ*(TMPAuxArray(2) + alphaQ*TmpArray2(2,2))
     TwoTerms(2) = inv2expQ*(TMPAuxArray(3) + alphaQ*TmpArray2(3,2))
     TwoTerms(3) = inv2expQ*(TMPAuxArray(4) + alphaQ*TmpArray2(4,2))
     do iTUV = 1,   10
      AuxArray(iTUV,iP) = AuxArray(iTUV,iP) + TMPAuxarray(iTUV)
     enddo
     AuxArray(11,iP) = AuxArray(11,iP) + Xqd*TMPAuxArray(5) + alphaXpq*TmpArray3(5,2) + 2*TwoTerms(1)
     AuxArray(12,iP) = AuxArray(12,iP) + Yqd*TMPAuxArray(5) + alphaYpq*TmpArray3(5,2)
     AuxArray(13,iP) = AuxArray(13,iP) + Zqd*TMPAuxArray(5) + alphaZpq*TmpArray3(5,2)
     AuxArray(14,iP) = AuxArray(14,iP) + Xqd*TMPAuxArray(8) + alphaXpq*TmpArray3(8,2)
     AuxArray(15,iP) = AuxArray(15,iP) + Xqd*TMPAuxArray(9) + alphaXpq*TmpArray3(9,2)
     AuxArray(16,iP) = AuxArray(16,iP) + Xqd*TMPAuxArray(10) + alphaXpq*TmpArray3(10,2)
     AuxArray(17,iP) = AuxArray(17,iP) + Yqd*TMPAuxArray(8) + alphaYpq*TmpArray3(8,2) + 2*TwoTerms(2)
     AuxArray(18,iP) = AuxArray(18,iP) + Zqd*TMPAuxArray(8) + alphaZpq*TmpArray3(8,2)
     AuxArray(19,iP) = AuxArray(19,iP) + Yqd*TMPAuxArray(10) + alphaYpq*TmpArray3(10,2)
     AuxArray(20,iP) = AuxArray(20,iP) + Zqd*TMPAuxArray(10) + alphaZpq*TmpArray3(10,2) + 2*TwoTerms(3)
   ENDDO !iPrimQ=1, nPrimQ
  ENDDO !iP = 1,nPrimP*nPassP
!$OMP END DO
 end subroutine
end module
