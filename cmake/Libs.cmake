
#in the long run only the lib should be built, no necessity for the executable
# The sources should also be compiled only once and in the order 
#meaning MAIN_SOURCES bepend on MAIN_ERI_SOURCES which depend on COMMON_MAIN_ERI_SOURCES
#which depend on COMMON_ICHORALGO_SOURCES an so on.

add_library(
    ichorlib
    ${PRECISION_SOURCES}
    ${AGC_SOURCES}
    ${COMMON_SOURCES}
    ${TRANSFER_SOURCES}
    ${COMMON_ICHORALGO_SOURCES}
    ${COMMON_MAIN_ERI_SOURCES}
    ${MAIN_ERI_SOURCES}
    ${MAIN_SOURCES}
    )

add_executable(
    ichorTest.x
    ${PRECISION_SOURCES}
    ${AGC_SOURCES}
    ${COMMON_SOURCES}
    ${TRANSFER_SOURCES}
    ${COMMON_ICHORALGO_SOURCES}
    ${COMMON_MAIN_ERI_SOURCES}
    ${MAIN_ERI_SOURCES}
    ${MAIN_SOURCES}
    ${CMAKE_SOURCE_DIR}/src/IchorTestingMain.F90
    )

#add_executable(
#    ichorProfTest.x
#    ${COMMON_SOURCES}
#    ${CMAKE_SOURCE_DIR}/src/IchorProfileMain.F90
#    )
