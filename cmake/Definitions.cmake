add_definitions(-DIMPLICIT_NONE)

if(cmake_build_type_tolower STREQUAL "debug")
  add_definitions(-DVAR_DEBUGICHOR)
endif()

if(HAVE_MKL_LAPACK)
    add_definitions(-DVAR_MKL)
endif()

if(ENABLE_64BIT_INTEGERS)
    add_definitions(-DVAR_INT64)
    #WARNING THIS IS TEMPORARY 
    #I know that the combi --int64 and MKL will result
    #in linking to a 64 bit integer lapack. To my 
    #knowledge no other combi will produce this
    if(HAVE_MKL_LAPACK)
      add_definitions(-DVAR_LAPACK_INT64)
    endif()
endif()

if(ENABLE_GPU)
    add_definitions(-DVAR_OPENACC)
    add_definitions(-DVAR_CUDA)
    if(ENABLE_CUBLAS)
        add_definitions(-DVAR_CUBLAS)
    endif()
endif()

if(ENABLE_TIMINGS)
    add_definitions(-DVAR_TIME)
endif()