macro(add_ichor_test _name _labels)
    
    add_test(NAME ${_name} COMMAND ${CMAKE_SOURCE_DIR}/test/IchorUnitTest/TEST -exe ${CMAKE_BINARY_DIR}/ichorTest.x -name ${_name}
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/test/IchorUnitTest)
    if(NOT "${_labels}" STREQUAL "")
        set_tests_properties(${_name} PROPERTIES LABELS "${_labels}")
    endif()
endmacro()

add_ichor_test(SSSS.test "SSSS")
add_ichor_test(all.test "all")
